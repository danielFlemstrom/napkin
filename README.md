# ⚠️ Repository Moved!
This repository is no longer maintained on Bitbucket.

🔗 **New Home:** [GitHub Repository](https://github.com/danielFlemstrom/napkin)

Please update your local repositories:
```bash
git remote set-url origin https://github.com/danielFlemstrom/napkin.git
